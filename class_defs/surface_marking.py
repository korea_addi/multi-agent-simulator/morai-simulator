﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
current_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.normpath(os.path.join(current_path, '../')))

from class_defs.base_plane import BasePlane

import numpy as np
from collections import OrderedDict

class SurfaceMarking(BasePlane):
    def __init__(self, points=None, idx=None):
        super(SurfaceMarking, self).__init__(points, idx)
        
        self.link_id_list = []
        self.road_id = ''

        self.link_list = list()

        self.type = None
        self.sub_type = None
        self.type_code_def = '' 

        self.plotted_obj = None

        self.reset_vis_mode_manual_appearance()


    def add_link_ref(self, link):
        if link not in self.link_list:
            self.link_list.append(link)
        
        if self not in link.surface_markings:
            link.surface_markings.append(self)


    def draw_plot(self, axes):
        if self.vis_mode_line_width is not None and \
            self.vis_mode_line_color is not None:
            self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                linewidth=self.vis_mode_line_width,
                color=self.vis_mode_line_color,
                markersize=1,
                marker='o')
            return
        
        else:
            self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                markersize=1,
                marker='o',
                color='b')


    def erase_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                if obj.axes is not None:
                    obj.remove()


    def hide_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                obj.set_visible(False)


    def unhide_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                obj.set_visible(True)            


    def set_vis_mode_manual_appearance(self, width, color):
        self.vis_mode_line_width = width
        self.vis_mode_line_color = color


    def reset_vis_mode_manual_appearance(self):
        self.set_vis_mode_manual_appearance(None, None)      


    @staticmethod
    def to_dict(obj):
        dict_data = {
            'idx': obj.idx,
            'points': obj.points.tolist(),
            'link_id_list': obj.link_id_list,
            'road_id' : obj.road_id,
            'type': obj.type,
            'sub_type': obj.sub_type
        }
        return dict_data


    @staticmethod
    def from_dict(dict_data, link_set=None):
        """STEP #1 파일 내 정보 읽기"""
        # 필수 정보
        idx = dict_data['idx']
        points = np.array(dict_data['points'])

        # 연결된 객체 참조용 정보
        link_id_list = dict_data['link_id_list']
        road_id = dict_data['road_id']

        # 기타 속성 정보
        sm_type = dict_data['type'] # type은 지정된 함수명이므로 혼란을 피하기 위해 sign_type으로
        sm_subtype = dict_data['sub_type']

        """STEP #2 인스턴스 생성"""
        obj = SurfaceMarking(points=points, idx=idx)

        # 연결된 객체 참조용 정보
        obj.link_id_list = link_id_list
        obj.road_id = road_id

        # 기타 속성 정보
        obj.type = sm_type
        obj.sub_type = sm_subtype

        """STEP #3 인스턴스 참조 연결"""
        if link_set is not None:
            for link_id in link_id_list:
                if link_id in link_set.lines.keys():
                    link = link_set.lines[link_id]
                    obj.add_link_ref(link)

        return obj


    def item_prop(self):
        prop_data = OrderedDict()
        prop_data['idx'] = {'type' : 'string', 'value' : self.idx}
        prop_data['points'] = {'type' : 'list<list<float>>', 'value' : self.points.tolist()}
        prop_data['type'] = {'type' : 'string', 'value' : self.type}
        prop_data['sub_type'] = {'type' : 'string', 'value' : self.sub_type}
        prop_data['type_code_def'] = {'type' : 'string', 'value' : self.type_code_def}
        return prop_data
