#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
current_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.normpath(os.path.join(current_path, '../')))

from class_defs.base_line import BaseLine

class Line(BaseLine):
    def __init__(self, points=None, idx=None):
        super(Line, self).__init__(points, idx)
        self.from_node = None
        self.to_node = None

        self.plotted_obj = None

        self.included_plane = list()

        self.set_vis_mode_all_different_color(False)
        self.reset_vis_mode_manual_appearance()

        self.geometry = [{'id':0, 'method':'poly3'}]


    def get_point_dict(self, point_idx):
        if point_idx == -1:
            point_idx = self.get_last_idx()

        if point_idx < 0:
            raise BaseException('[ERROR] Line.get_point_dict: input argument point_idx must be >= 0. (-1 is exceptionally ok).')

        if point_idx == 0:
            type_str = 'start'
        elif point_idx == self.get_last_idx():
            type_str = 'end'
        else:
            type_str = 'mid'
        
        dict_obj = dict({
            'idx_line': self.idx,
            'idx_point': point_idx,
            'type': type_str,
            'coord': self.points[point_idx],
            'line_ref': self})
        return dict_obj 
    

    def get_last_idx(self):
        return self.points.shape[0] - 1
    

    def set_from_node(self, _from_node):
        if self.from_node is not None:
            self.from_node.remove_to_links(self)

        self.from_node = _from_node

        if _from_node is not None and self not in _from_node.to_links:
            _from_node.to_links.append(self)


    def set_to_node(self, _to_node):
        if self.to_node is not None:
            self.to_node.remove_from_links(self)

        self.to_node = _to_node

        if _to_node is not None and self not in _to_node.from_links:
            _to_node.from_links.append(self)


    def remove_from_node(self):
        self.from_node.remove_to_links(self)
        self.from_node = None
    
    
    def remove_to_node(self):
        self.to_node.remove_from_links(self)
        self.to_node = None
        

    def get_from_node(self):
        return self.from_node 


    def get_to_node(self):
        return self.to_node


    def get_from_links(self):
        return self.from_node.get_from_links()
    

    def get_to_links(self):
        return self.to_node.get_to_links()


    def get_from_node_sharing_links(self):
        links = self.from_node.get_to_links()
        ret = list()
        for each_link in links:
            if each_link is not self:
                ret.append(each_link)
        return ret


    def get_to_node_sharing_links(self):
        links = self.to_node.get_from_links()
        ret = list()
        for each_link in links:
            if each_link is not self:
                ret.append(each_link)
        return ret


    def is_source(self):
        return len(self.get_from_links()) == 0


    def is_sink(self):
        return len(self.get_to_links()) == 0


    def get_included_planes(self):
        return self.included_plane


    def add_included_plane(self, plane):
        self.included_plane.append(plane)


    def remove_included_plane(self, plane_to_remove):
        self.included_plane.remove(plane_to_remove)


    def add_geometry(self, point_id, method):
        if point_id == len(self.points) - 1:
            raise BaseException('adding geometry point in the last point is not supported.')
        for geo_point in self.geometry:
            if geo_point['id'] == point_id:
                geo_point['method'] = method
                return
        self.geometry.append({'id':point_id, 'method':method})
        self.geometry = sorted(self.geometry, key=lambda element: element['id'])


    def draw_plot(self, axes):
        if self.vis_mode_line_width is not None and \
            self.vis_mode_line_color is not None:
            self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                linewidth=self.vis_mode_line_width,
                color=self.vis_mode_line_color,
                markersize=1,
                marker='o')
            return
        

        if self.get_vis_mode_all_different_color():
            self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                markersize=1,
                marker='o')
        
        else:
            if not self.included_plane:
                self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                    markersize=1,
                    marker='o',
                    color='k')
            else:
                self.plotted_obj = axes.plot(self.points[:,0], self.points[:,1],
                    markersize=1,
                    marker='o',
                    color='b')


    def erase_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                if obj.axes is not None:
                    obj.remove()


    def hide_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                obj.set_visible(False)


    def unhide_plot(self):
        if self.plotted_obj is not None:
            for obj in self.plotted_obj:
                obj.set_visible(True)


    def set_vis_mode_all_different_color(self, on_off):
        self.vis_mode_all_different_color = on_off


    def get_vis_mode_all_different_color(self):
        return self.vis_mode_all_different_color


    def set_vis_mode_manual_appearance(self, width, color):
        self.vis_mode_line_width = width
        self.vis_mode_line_color = color


    def reset_vis_mode_manual_appearance(self):
        self.set_vis_mode_manual_appearance(None, None)

                
                
