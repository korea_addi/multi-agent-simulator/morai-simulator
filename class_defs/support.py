﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
#확인
'''
The supported_class variable is a project specific indicator used by the loading
system to determine what .json files to search for and what classes to
initialize
'''

supported_class = {
    'synced_signal' : True,
    'intersection_controller' : True
}
