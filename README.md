# MORAI Simulator


## Getting started

This code base is a sample MGeo class structure allowing users to save and load their own MGeo datasets.
- Use subproc2 within the save_load directory to load MGeo .json data
- Generate lane mark mesh objects with edit/edit_lane_boundary.py
- Save your datasets using the to_json() function within mgeo.py 