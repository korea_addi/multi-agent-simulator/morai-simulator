﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np  

def create_mesh_gen_points(lane_boundary, solid_line_interval=0.5):
    if lane_boundary.get_lane_num() == 1:
        return create_mesh_gen_points_single_lane(lane_boundary, solid_line_interval)
    else:
        return create_mesh_gen_points_double_lane(lane_boundary, solid_line_interval)


def create_mesh_gen_points_single_lane(lane_boundary, solid_line_interval=0.5):
    lane_boundary.mesh_gen_vertices = []
    lane_boundary.mesh_gen_vertex_subsets_for_each_face = []
    lane_boundary.mesh_gen_vertex_uv_coords = []

    point_step = 0.2
    below_exec = False

    solid_only_step = int(solid_line_interval/point_step)

    lane_shape = lane_boundary.lane_shape
    if lane_shape[0].lower() == 'broken':
        dash_interval_L1 = lane_boundary.dash_interval_L1
        dash_interval_L2 = lane_boundary.dash_interval_L2
        L1L2 = dash_interval_L1 + dash_interval_L2
        if L1L2 == 0.0:
            print('lane marking init has problem')
            return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords

        total_length = len(lane_boundary.points) * point_step
        total_num_set = int(np.floor(total_length / L1L2))
        if total_num_set == 0: # delta 계산을 위해 total_num_set으로 나눠야하므로 계산 필요
            print('lane: {} is too short to create a mesh (lane code: {}, total len: {:.2f}, L1: {}, L2: {})'.format(lane_boundary.idx, lane_boundary.lane_type, total_length, dash_interval_L1, dash_interval_L2))
            return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords
        remainder = total_length % L1L2
        delta = remainder / (total_num_set * 2)

        dash_interval_L1 += delta
        dash_interval_L2 += delta

        dashed_step_L1 = int(np.ceil(dash_interval_L1/point_step))
        dashed_step_L2 = int(np.floor(dash_interval_L2/point_step))

        below_exec = True

    i = 0
    face_cnt = 0
    paint = True # 점선일 때, 그리는 구간 / 넘어가는 구간을 토글링하기 위한 변수
    uv_idx = 0 # uv 좌표 카운팅.
    once_flag = True

    if lane_shape[0].lower() != 'broken':
        remain_points = len(lane_boundary.points) % solid_only_step
        remain_ratio = remain_points / solid_only_step

    slice_count = -1
    while i < len(lane_boundary.points):
        if i == len(lane_boundary.points) - 1:                   # 마지막 값이면, 이전 값과의 차이로 계산
            vect = lane_boundary.points[i] - lane_boundary.points[i-1]
        elif i != 0 and i < len(lane_boundary.points) - 1:       # 중간 각을 위해 앞, 뒤 벡터 를 이용하여 계산.
            vect = lane_boundary.points[i+1] - lane_boundary.points[i-1]
        else:                                           # index = 0
            vect = lane_boundary.points[i+1] - lane_boundary.points[i]
        vect = vect / np.linalg.norm(vect, ord=2)

        pos_vect_ccw = lane_boundary.rorate_around_z_axis(np.pi/2, vect)
        pos_vect_cw = lane_boundary.rorate_around_z_axis(-np.pi/2, vect)

        pos_vect_ccw[2] = 0
        pos_vect_cw[2] = 0

        lane_width_ratio = 1.0
        if check_inflection_point(lane_boundary, False, solid_only_step, i):      # 곡선 구간 up, dn 버텍스 간격의 길이 보간
            lane_width_ratio = 1.414 # 루트2
            if i != 0 and i != len(lane_boundary.points) - 1:
                before_vec = lane_boundary.points[i - 1] - lane_boundary.points[i]
                after_vec = lane_boundary.points[i + 1] - lane_boundary.points[i]
                dot_value = calc_dot_product(before_vec, after_vec)
                bet_radian = np.emath.arccos(dot_value)
                bet_degree = bet_radian * 180 / np.math.pi
                sin_value = np.sin(bet_radian)
                if bet_degree < 50: sin_value = sin_value ** 2
                lane_width_ratio = lane_width_ratio * sin_value

        pos_vect_ccw = pos_vect_ccw * lane_boundary.lane_width * lane_width_ratio / 2.0
        pos_vect_cw = pos_vect_cw * lane_boundary.lane_width * lane_width_ratio / 2.0

        up = (lane_boundary.points[i] + pos_vect_ccw).tolist()
        dn = (lane_boundary.points[i] + pos_vect_cw).tolist()

        lane_boundary.mesh_gen_vertices.append(up)
        lane_boundary.mesh_gen_vertices.append(dn)

        if lane_shape[0].lower() != 'broken':
            i += solid_only_step
            if i >= len(lane_boundary.points) and once_flag:
                i = len(lane_boundary.points) - 1
                once_flag = False
        else:
            if not below_exec:
                print('[WARNING] May cause issues')
            if paint:
                i += dashed_step_L1
            else:
                i += dashed_step_L2

        if face_cnt > 0: # 첫번째 loop에는 하지 않기 위함
            if lane_boundary.lane_shape[0].lower() == 'solid' or paint:
                start_id = (face_cnt - 1) * 2
                face = [start_id, start_id+1, start_id+3, start_id+2]

                lane_boundary.mesh_gen_vertex_subsets_for_each_face.append(face)

        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 0])
        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 1])

        if lane_shape[0].lower() == 'solid':
            if i == len(lane_boundary.points) - 1:
                uv_idx += (1 - remain_ratio)
            else:
                if slice_count >= 0:
                    uv_idx += 1 / 5
                else:
                    uv_idx += 1
        else:
            if paint:
                uv_idx = 0
            else:
                uv_idx = 6

        face_cnt += 1 # face_cnt는 실제 paint를 칠한 것 여부와 관계없이 증가
        paint = not paint # paint 칠했으면 다음번엔 칠하지 않아야하므로 토글

        if check_inflection_point(lane_boundary, True, solid_only_step, i):
            solid_only_step = int(solid_line_interval/0.5)
            slice_count = int(solid_line_interval/point_step) + 10 # 크게 잡아서 더 부드럽게 보이도록 하였습니다.
        else:
            slice_count -= 1
            if slice_count < 0:
                solid_only_step = int(solid_line_interval/point_step)

    return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords


def create_mesh_gen_points_double_lane(lane_boundary, solid_line_interval=0.5):
    lane_boundary.mesh_gen_vertices = []
    lane_boundary.mesh_gen_vertex_subsets_for_each_face = []
    lane_boundary.mesh_gen_vertex_uv_coords = [] # uv 좌표

    point_step = 0.1
    below_exec = False

    solid_only_step = int(solid_line_interval/point_step)

    lane_shape = lane_boundary.lane_shape
    if 'Broken' in lane_shape or 'broken' in lane_shape:
        dash_interval_L1 = lane_boundary.dash_interval_L1
        dash_interval_L2 = lane_boundary.dash_interval_L2
        L1L2 = dash_interval_L1 + dash_interval_L2
        if L1L2 == 0.0:
            print('lane marking init has problem')
            return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords

        total_length = len(lane_boundary.points) * point_step
        total_num_set = int(np.floor(total_length / L1L2))
        if total_num_set == 0: # delta 계산을 위해 total_num_set으로 나눠야하므로 계산 필요
            print('lane: {} is too short to create a mesh (lane code: {}, total len: {:.2f}, L1: {}, L2: {})'.format(lane_boundary.idx, lane_boundary.lane_type, total_length, dash_interval_L1, dash_interval_L2))
            return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords
        remainder = total_length % L1L2
        delta = remainder / (total_num_set * 2)

        dash_interval_L1 = dash_interval_L1 + delta
        dash_interval_L2 = dash_interval_L2 + delta

        dashed_step_L1 = int(np.ceil(dash_interval_L1/point_step))
        dashed_step_L2 = int(np.floor(dash_interval_L2/point_step))

        below_exec = True

    i = 0
    face_cnt = 0
    paint = True # 점선일 때, 그리는 구간 / 넘어가는 구간을 토글링하기 위한 변수
    uv_idx = 0 # uv 좌표 카운팅.

    while i < len(lane_boundary.points):
        if i == len(lane_boundary.points) - 1: # 마지막 값이면, 이전 값과의 차이로 계산
            vect = lane_boundary.points[i] - lane_boundary.points[i-1]
        else:
            vect = lane_boundary.points[i+1] - lane_boundary.points[i]
        vect = vect / np.linalg.norm(vect, ord=2)

        pos_vect_ccw = lane_boundary.rorate_around_z_axis(np.pi/2, vect)
        pos_vect_cw = lane_boundary.rorate_around_z_axis(-np.pi/2, vect)

        pos_vect_ccw[2] = 0
        pos_vect_cw[2] = 0

        pos_vect_ccw1 = pos_vect_ccw * (lane_boundary.double_line_interval / 2.0 + lane_boundary.lane_width)
        pos_vect_ccw2 = pos_vect_ccw * (lane_boundary.double_line_interval / 2.0)
        pos_vect_cw1 = pos_vect_cw * (lane_boundary.double_line_interval / 2.0)
        pos_vect_cw2 = pos_vect_cw * (lane_boundary.double_line_interval / 2.0 + lane_boundary.lane_width)

        up1 = (lane_boundary.points[i] + pos_vect_ccw1).tolist()
        up2 = (lane_boundary.points[i] + pos_vect_ccw2).tolist()
        dn1 = (lane_boundary.points[i] + pos_vect_cw1).tolist()
        dn2 = (lane_boundary.points[i] + pos_vect_cw2).tolist()

        lane_boundary.mesh_gen_vertices.append(up1)
        lane_boundary.mesh_gen_vertices.append(up2)
        lane_boundary.mesh_gen_vertices.append(dn1)
        lane_boundary.mesh_gen_vertices.append(dn2)

        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 0])
        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 1])
        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 0])
        lane_boundary.mesh_gen_vertex_uv_coords.append([uv_idx, 1])

        if ('Broken' in lane_shape or 'broken' in lane_shape) and ('Solid' in lane_shape or 'solid' in lane_shape):
            uv_idx += 6
        else:
            uv_idx += 1

        if 'Broken' not in lane_shape and 'broken' not in lane_shape:
            i += solid_only_step
        else:
            if not below_exec:
                print('May cause issues')
            if paint:
                i += dashed_step_L1
            else:
                i += dashed_step_L2

        if face_cnt > 0: # 첫번째 loop에는 하지 않기 위함
            start_id = (face_cnt - 1) * 4
            face1 = [start_id+0, start_id+1, start_id+5, start_id+4]
            face2 = [start_id+2, start_id+3, start_id+7, start_id+6]

            if lane_shape[0].lower() == 'solid' or paint: # solid 일 때는 항상 paint = True이므로, lane_shape는 신경쓰지 않아도 된다.
                lane_boundary.mesh_gen_vertex_subsets_for_each_face.append(face1)

            if lane_shape[1].lower() == 'solid' or paint:
                lane_boundary.mesh_gen_vertex_subsets_for_each_face.append(face2)

        face_cnt += 1 # face_cnt는 실제 paint를 칠한 것 여부와 관계없이 증가
        paint = not paint # paint 칠했으면 다음번엔 칠하지 않아야하므로 토글

    return lane_boundary.mesh_gen_vertices, lane_boundary.mesh_gen_vertex_subsets_for_each_face, lane_boundary.mesh_gen_vertex_uv_coords


def calc_dot_product(vec_1, vec_2):
    vect_1_norm = vec_1 / np.linalg.norm(vec_1, ord=2)
    vect_2_norm = vec_2 / np.linalg.norm(vec_2, ord=2)
    dot_value = np.dot(vect_1_norm, vect_2_norm)
    if dot_value > 1.0 : dot_value = 1.0
    elif dot_value < -1.0 : dot_value = -1.0

    return dot_value


def get_gradient_first_node(lane_boundary, point_idx, solid_step):
    if point_idx == len(lane_boundary.points) - 1:
        return -1
    elif point_idx + solid_step >= len(lane_boundary.points) - 1:
        return -1
    elif point_idx + (solid_step * 2) >= len(lane_boundary.points) - 1:
        return -1
    else:
        vect_1 = lane_boundary.points[point_idx] - lane_boundary.points[point_idx + solid_step]
        vect_2 = lane_boundary.points[point_idx + (solid_step * 2)] - lane_boundary.points[point_idx + solid_step]
        dot_value = calc_dot_product(vect_1, vect_2)
        return dot_value


def get_gradient_center_node(lane_boundary, point_idx, solid_step):
    if point_idx == len(lane_boundary.points) - 1:
        return -1
    elif point_idx + solid_step >= len(lane_boundary.points) - 1:
        return -1
    if point_idx - solid_step < 0:
        return -1
    else:
        vect_1 = lane_boundary.points[point_idx - solid_step] - lane_boundary.points[point_idx]
        vect_2 = lane_boundary.points[point_idx + solid_step] - lane_boundary.points[point_idx]
        dot_value = calc_dot_product(vect_1, vect_2)
        return dot_value


def check_inflection_point(lane_boundary, is_first_node, cur_solid_only_step, cur_idx):
    if lane_boundary.lane_shape[0].lower() == 'solid' and (cur_idx < len(lane_boundary.points) - 1) and (cur_idx > 0):
        dot_value = 0

        if is_first_node:
            dot_value = get_gradient_first_node(lane_boundary, cur_idx, cur_solid_only_step)
        else:
            dot_value = get_gradient_center_node(lane_boundary, cur_idx, cur_solid_only_step)

        next_radian = np.emath.arccos(dot_value)
        next_degree = next_radian * 180 / np.math.pi
        threshold_degree = 150

        if ((next_degree < threshold_degree) or (next_degree > 360 - threshold_degree)):
            return True

    return False
